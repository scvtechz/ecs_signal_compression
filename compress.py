import scipy.io
import scipy.fftpack
import subprocess
import struct
import os
import time

def extract_data(mat_data, dict_name):
    mat_data_selection = mat_data[dict_name]
    result = []
    for i in range(len(mat_data_selection) - 2):
        result.append((mat_data_selection[i])[0])
    return result


def save_data(data, filename, repeat = 1 ):
    with open(filename, 'wb') as file_handler:
        for i in range(0,repeat):
            for entry in data:
                file_handler.write(struct.pack('>H',entry))
        return file_handler.tell()


def compress(uncompressed_data):
    result = []

    data_slice = uncompressed_data[0:63]

    result = scipy.fftpack.dct(data_slice)

    return result


def uncompress(compressed_data):
    result = []

    result = scipy.fftpack.idct(compressed_data)

    return result


def encode_flac(filename):
    size = 0
    enc_time = 0
    dec_time = 0

    filename_enc = "{}.flac_enc".format(filename)

    p = subprocess.Popen(args="bin/flac/flac.exe --force --endian=big --bps=16 --channels=1 --sample-rate=1024 --sign=unsigned --force-raw-format -o {0} {1}".format(filename_enc, filename))
    enc_st = time.time()
    ret = p.wait()
    enc_ed = time.time()

    enc_time = enc_ed - enc_st

    filename_dec = "{}.flac_dec".format(filename)

    p = subprocess.Popen(args="bin/flac/flac.exe -d --force -o {0} {1}".format(filename_dec, filename_enc))
    dec_st = time.time()
    ret = p.wait()
    dec_ed = time.time()

    dec_time = dec_ed - dec_st

    size = os.path.getsize(filename_enc)

    return (size, enc_time, dec_time)

def encode_lzop(filename):
    size = 0
    enc_time = 0
    dec_time = 0

    filename_enc = "{}.lzop_enc".format(filename)

    p = subprocess.Popen(args="bin/lzop/lzop.exe -f -o {1} {0}".format(filename, filename_enc))
    enc_st = time.time()
    ret = p.wait()
    enc_ed = time.time()

    enc_time = enc_ed - enc_st

    filename_dec = "{}.lzop_dec".format(filename)

    p = subprocess.Popen(args="bin/lzop/lzop.exe -fd -o {1} {0}".format(filename_enc, filename_dec))
    dec_st = time.time()
    ret = p.wait()
    dec_ed = time.time()

    dec_time = dec_ed - dec_st

    size = os.path.getsize(filename_enc)

    return (size, enc_time, dec_time)

def encode_lzo(filename):
    size = 0
    enc_time = 0
    dec_time = 0

    filename_enc = "{}.lzo_enc".format( filename )

    p = subprocess.Popen(args="bin/mini_lzo/lzo_cli.exe encode {0} {1}".format(filename, filename_enc))
    enc_st = time.time()
    ret = p.wait()
    enc_ed = time.time()

    enc_time = enc_ed - enc_st

    filename_dec = "{}.lzo_dec".format( filename )

    p = subprocess.Popen(args="bin/mini_lzo/lzo_cli.exe decode {0} {1}".format(filename_enc, filename_dec))
    dec_st = time.time()
    ret = p.wait()
    dec_ed = time.time()

    dec_time = dec_ed - dec_st

    size = os.path.getsize( filename_enc )

    return (size, enc_time, dec_time)

def encode_gzip(filename):
    size = 0
    enc_time = 0
    dec_time = 0

    filename_enc = "{}.gz".format( filename )

    p = subprocess.Popen(args="bin\gzip\gzip.exe -f {}".format(filename) )
    enc_st = time.time()
    ret = p.wait()
    enc_ed = time.time()

    enc_time = enc_ed - enc_st

    size = os.path.getsize(filename_enc)

    p = subprocess.Popen(args="bin\gzip\gzip.exe -f -d {}".format(filename_enc))
    dec_st = time.time()
    ret = p.wait()
    dec_ed = time.time()

    dec_time = dec_ed - dec_st

    return (size, enc_time, dec_time)

def test_compression(file_name, entry_name):
    mat = scipy.io.loadmat(file_name)

    data = extract_data(mat, entry_name)

    files = []
    files_raw = []

    for i in [1, 10, 100, 1000]:
        file_name = "file_{}.data".format(i)
        size = save_data( data, file_name, i)
        files_raw.append(( file_name, size))
        files.append( file_name )

    encoded_gzip = []
    for file in files:
        (size, enc_time, dec_time) = encode_gzip(file)
        encoded_gzip.append((file, size, enc_time, dec_time))


    encoded_lzo = []
    for file in files:
        (size, enc_time, dec_time) = encode_lzo(file)
        encoded_lzo.append((file, size, enc_time, dec_time))


    encoded_lzop = []
    for file in files:
        (size, enc_time, dec_time) = encode_lzop(file)
        encoded_lzop.append((file, size, enc_time, dec_time))

    encoded_flac = []

    for file in files:
        (size,enc_time, dec_time) = encode_flac(file)
        encoded_flac.append((file, size, enc_time, dec_time))

    print("test_done: {}".format(entry_name))

    print("UNCOMPRESSED:")
    for file_name,size in files_raw:
        print( "{0} size: {1}".format(file_name, size) )

    print("LZO:")
    for file_name, size, enc_time, dec_time in encoded_lzo:
        print( "{0} size: {1} enc: {2} dec: {3}".format(file_name, size, enc_time, dec_time) )

    print("LZOP:")
    for file_name, size, enc_time, dec_time in encoded_lzop:
        print("{0} size: {1} enc: {2} dec: {3}".format(file_name, size, enc_time, dec_time))

    print("GZIP(DEFLATE):")
    for file_name, size, enc_time, dec_time in encoded_gzip:
        print("{0} size: {1} enc: {2} dec: {3}".format(file_name, size, enc_time, dec_time))

    print("FLAC:")
    for file_name, size, enc_time, dec_time in encoded_flac:
        print("{0} size: {1} enc: {2} dec: {3}".format(file_name, size, enc_time, dec_time))

def run_tests():
    test_cases = ["rawECS_200SPS_10bit"]

    for test_case in test_cases:
        test_compression("rawECS.mat", test_case)


if __name__ == '__main__':
    run_tests()
