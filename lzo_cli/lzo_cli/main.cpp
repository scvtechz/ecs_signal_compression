#include<iostream>
#include<fstream>
#include<memory>
#include<algorithm>
#include<vector>

#include"minilzo.h"

enum class Command {
	Encode,
	Decode,
	Incorrect
};

struct PackedCommand {
	std::string input_filename ;
	std::string output_filename ;
	Command command ;
};

PackedCommand ParseCommand(int argc, char**argv) {
	PackedCommand result ;
	result.command = Command::Incorrect ;

	if (argc < 4) {
		return ( result ) ;
	}

	std::string command_name = argv[1]; 

	if (command_name == "encode") {
		result.command = Command::Encode ;
		result.input_filename = argv[2] ;
		result.output_filename = argv[3] ;
	}
	else if (command_name == "decode") {
		result.command = Command::Decode ;
		result.input_filename = argv[2] ;
		result.output_filename = argv[3] ;
	}
	return ( result ) ;
}

int Encode(const std::string input_filename, const std::string output_filename ) {
	int result = 0 ;

	std::ifstream input_file ;
	std::ofstream output_file ;

	input_file.open( input_filename, std::ifstream::binary|std::ifstream::in ) ;

	if (!input_file.is_open()) {
		result = -1 ;
		return ( result ) ;
	}

	output_file.open( output_filename, std::ofstream::binary|std::ofstream::out ) ;

	if (!output_file.is_open()) {
		result = -1 ;
		return ( result ) ;
	}

	std::streamsize step_size { 4096 } ;

	std::vector<unsigned char> input_buffer( step_size ) ;
	std::vector<unsigned char> output_buffer( step_size * 2 ) ;
	std::vector<lzo_align_t> work_mem(((LZO1X_MEM_COMPRESS)+(sizeof(lzo_align_t) - 1)) / sizeof(lzo_align_t)) ;

	input_file.seekg(0,std::ifstream::end) ;
	std::streamsize stream_size = input_file.tellg() ;
	input_file.seekg(std::ifstream::beg) ;
		
	std::streamsize stream_pos = 0 ;
	
	while ( stream_pos < stream_size) {
		std::streamsize read = std::min(stream_size - stream_pos, std::streamsize{ step_size }) ;

		input_file.read( reinterpret_cast<char*>(input_buffer.data()), read ) ;

		lzo_uint encoded = 0 ;
		int enc_result = lzo1x_1_compress( input_buffer.data(), read, output_buffer.data(), &encoded , work_mem.data() ) ;

		stream_pos += read ;

		if (enc_result != 0) {
			result = -1 ;
			break ;
		}

		if (encoded) {
			output_file.write(reinterpret_cast<char*>(&encoded), sizeof(encoded) ) ;
			output_file.write( reinterpret_cast<char*>(output_buffer.data()), encoded ) ;
		}
	}

	input_file.close() ;
	output_file.close() ;

	return ( result ) ;
}

int Decode(const std::string input_filename, const std::string output_filename) {
	int result = 0 ;

	std::ifstream input_file ;
	std::ofstream output_file ;

	input_file.open(input_filename, std::ifstream::binary | std::ifstream::in) ;

	if (!input_file.is_open()) {
		result = -1 ;
		return (result) ;
	}

	output_file.open(output_filename, std::ofstream::binary | std::ofstream::out) ;

	if (!output_file.is_open()) {
		result = -1 ;
		return (result) ;
	}

	std::streamsize step_size{ 4096 } ;

	std::vector<unsigned char> input_buffer(step_size * 2) ;
	std::vector<unsigned char> output_buffer(step_size ) ;

	input_file.seekg(0, std::ifstream::end) ;
	std::streamsize stream_size = input_file.tellg() ;
	input_file.seekg(std::ifstream::beg) ;

	std::streamsize stream_pos = 0 ;

	while (stream_pos < stream_size) {
		lzo_uint read_size = 0 ;

		input_file.read( reinterpret_cast<char*>(&read_size), sizeof(lzo_uint) ) ;

		std::streamsize read{ read_size };

		input_file.read(reinterpret_cast<char*>(input_buffer.data()), read) ;

		lzo_uint decoded = 0 ;

		int enc_result = lzo1x_decompress(input_buffer.data(), read, output_buffer.data(), &decoded, 0) ;

		stream_pos += read ;

		if (enc_result != 0) {
			result = -1 ;
			break ;
		}

		if (decoded) {
			output_file.write(reinterpret_cast<char*>(output_buffer.data()), decoded) ;
		}
	}

	input_file.close() ;
	output_file.close() ;

	return ( result ) ;
}

int Incorrect() {
	std::cout<< "Incorrect command. Expected: encode|decode input_file_name output_file_name" << std::endl ;

	return ( -1 ) ;
}

int ExecuteCommand(PackedCommand command) {
	int result = -1 ;
	switch (command.command)
	{
	case Command::Encode:
		result = Encode( command.input_filename, command.output_filename ) ;
		break;
	case Command::Decode:
		result = Decode( command.input_filename, command.output_filename ) ;
		break;
	case Command::Incorrect:
		result = Incorrect() ;
		break;
	default:
		result = Incorrect() ;
		break;
	}

	return ( result ) ;
}

int main(int argc, char** argv) {
	int32_t result = 0 ;

	PackedCommand command = ParseCommand( argc, argv ) ;

	result = ExecuteCommand ( command ) ;

	return ( result ) ;
}